CREATE TABLE application(
id int(10) unsigned NOT NULL AUTO_INCREMENT,
login VARCHAR(128) NOT NULL DEFAULT'',
mail VARCHAR(128) NOT NULL DEFAULT'',
date int(4) NOT NULL, 
sex VARCHAR(4) NOT NULL DEFAULT'',
amount int(10) NOT NULL DEFAULT 0,
superpowers text(128) NOT NULL,
biography text(128) NOT NULL,
contract text(128) NOT NULL DEFAULT 'no',
PRIMARY KEY(id)
);
